/*function onLoad() {
	document.getElementById("firstname").addEventListener("onkeyup", fnameValidate());
	document.getElementById("lastname").addEventListener("onkeyup", lnameValidate());
}*/
function fnameValidate() {
	var valid = /^[A-Za-z]+$/;
	var field = document.forms["form"]["firstname"].value;
	if(field.match(valid))
		return true;
	else {
		document.getElementById("fnameAlert").innerHTML = "Name must be letters only.";
		return false;
	}
}

function lnameValidate() {
	var valid = /^[A-Za-z]+$/;
	var field = document.forms["form"]["lastname"].value;
	if(field.match(valid))
		return true;
	else 
	{
		document.getElementById("lnameAlert").innerHTML = "Name must be letters only.";
		return false;
	}
}

function unameValidate() {
	var valid = /^(?=.{5,12})[a-zA-Z0-9]+$/;
	var field = document.forms["form"]["username"].value;
	if(field.match(valid))
		return true;
	else
	{
		document.getElementById("unameAlert").innerHTML = "Username must be 5-12 alphanumeral characters.";
		return false;
	}
}

function pwValidate() {
	var valid = /^(?=.{5,12})(?=.*d)(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/;
	var field = document.forms["form"]["pword"].value;
	if(field.match(valid))
		return true;
	else
	{
		document.getElementById("passAlert").innerHTML = "Password must be 5-12 characters with at least one lowercase and capital letter, one number, and one special character.";
		return false;
	}
}

function pwconfirmValidate() {
	var field = document.forms["form"]["pwordconfirm"].value;
	if(field == document.forms["form"]["pword"].value)
		return true;
	else
	{
		document.getElementById("passConAlert").innerHTML = "Passwords must match.";
		return false;
	}
}

function emailConValidate() {
	var field = document.forms["form"]["emailconfirm"].value;
	if(field == document.forms["form"]["email"].value)
		return true;
	else
	{
		document.getElementById("emailConAlert").innerHTML = "Email addresses must match.";
		return false;
	}
}

function phoneValidate() {
	var valid = /^(?([0-9]{3}))?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	var field = document.forms["form"]["phone"].value;
	if(field.match(valid))
		return true;
	else
	{
		document.getElementById("phoneAlert").innerHTML = "Password must be 5-12 characters with at least one lowercase and capital letter, one number, and one special character.";
		return false;
	}
}

function adrValidate() {
	var valid = /^([0-9])([a-zA-Z. ])$/;
	var field = document.forms["form"]["address"].value;
	if(field.match(valid))
		return true;
	else
	{	
		document.getElementById("adrAlert").innerHTML = "Invalid address format.";
		return false;
	}
}

function stateValidate() {
	var valid = /^(?:A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])*$/;
	var field = document.forms["form"]["state"].value;
	if(field.match(valid))
		return true;
	else
	{
		document.getElementById("stAlert").innerHTML = "Invalid state two-letter code.";
		return false;
	}
}

function zipValidate() {
	var valid = /^([0-9]{5})[-]?([0-9]{4})?*$/;
	var field = document.forms["form"]["state"].value;
	if(field.match(valid))
		return true;
	else
	{
		document.getElementById("zipAlert").innerHTML = "Invalid ZIP Code.";
		return false;
	}
}

function onFormSubmit(e) {
	var retValue = false;

	if(!(zipValidate() | stateValidate() | fnameValidate() | lnameValidate() | phoneValidate() | emailConValidate() | pwconfirmValidate() | pwValidate() | adrValidate() | unameValidate()))
	{
		retValue = true;
		document.getElementById("form").submit();
	}
	else
	{
		e.preventDefault();
		retValue = false;
	}

	return retValue;
}